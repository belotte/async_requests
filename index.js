const request =	require ('request')

const make_request = (creation_method) => {
	return ({ method, url, headers, auth, body, form, rejectUnauthorized, formData, requestCert, agent } = { }) => {
		return new Promise ((resolve, reject) => {
			return request ({
				url,
				method: creation_method || method,
				...(auth !== undefined ? { auth } : {}),
				...(headers !== undefined ? { headers } : {}),
				json: true,
				...(rejectUnauthorized !== undefined ? { rejectUnauthorized } : {}),
				...(requestCert !== undefined ? { requestCert } : {}),
				...(agent !== undefined ? { agent } : {}),
				...(body !== undefined ? { body } : {}),
				...(form !== undefined ? { form } : {}),
				...(formData !== undefined ? { formData } : {}),
			}, (error, response, body) => {
				return resolve ({ error, response, body, status_code: (response || { }).statusCode })
			})
		})
	}
}

module.exports = {
	make_request,
	post: make_request ('post'),
	get: make_request ('get'),
	put: make_request ('put'),
	delete: make_request ('delete'),
	custom: make_request ()
}

