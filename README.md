### Available parameters
``` javascript
{
	url: 'http://google.fr',
	method: 'GET', // cant be set if a method was provided in `make_request`
	json: true, // cant be changed
	auth: {},
	headers: {
		Authorization: `Bearer ...`
	},
	rejectUnauthorizedL {},
	requestCert: {},
	agent: {},
	body: {
		x: 42
	},
	form: {},
	formData: {}
}
```

### Utilisation
``` javascript
let {
    make_request,
    post,
    get,
    put,
    del,
    custom
} = require ('async_requests')

async function get_google () {
    let { error, response, body, status_code } = await get ({
        url: 'http://google.fr'
    })

    console.log (body)
}
```

##### Load as global
``` javascript
const { make_request } = require (`async_requests`)

global.requests = {
	post: make_request ('post'),
	get: make_request ('get'),
	put: make_request ('put'),
	delete: make_request ('delete'),
	custom: make_request ()
}
```
